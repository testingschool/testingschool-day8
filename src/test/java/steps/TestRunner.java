package steps;

import configuration.AutomationConfig;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by Marius Dima on 26.08.2019.
 */

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "json:target/cucumber.json"},
        features = "src/test/resources/features", glue = "steps", extraGlue = "api")
@ContextConfiguration(classes = AutomationConfig.class)
@ActiveProfiles("api")
public class TestRunner {
}
