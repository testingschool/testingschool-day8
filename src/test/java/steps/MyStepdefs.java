package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ActiveProfiles;
import pageobjects.ContactFormPage;
import pageobjects.HomePage;
@Scope("cucumber-glue")
public class MyStepdefs extends TestRunner {

    @Autowired
    private WebDriver driver;

    @Value("${browser.url:}")
    private String homepageUrl;

    private Object currentPage;

    @Given("I navigate to {string}")
    public void iNavigateTo(String url) {
        driver.get(homepageUrl);
        currentPage = new HomePage(driver);
    }

    @When("^I click on contact link$")
    public void iClickOnContactLink() {
        currentPage = ((HomePage) currentPage).clickContactLink();
    }

    @And("^I complete contact details$")
    public void iCompleteContactDetails() {
        ContactFormPage contactFormPage = (ContactFormPage) currentPage;
        contactFormPage.selectSubjectHeading("Customer service");
        contactFormPage.inputEmail("test@test.com");
        contactFormPage.inputMessage(" My Message");
    }

    @And("^I submit contact form$")
    public void iSubmitContactForm() {
        ((ContactFormPage) currentPage).clickSubmit();
    }

    @Then("^I get a contact form submitted$")
    public void iGetAContactFormSubmitted() {
        driver.quit();
    }


}
