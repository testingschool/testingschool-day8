package backpack;

import org.springframework.stereotype.Component;

/**
 * Created by Marius Dima on 05.09.2019.
 */
@Component
public class TestBackpack {

    private String email;
    private String password;
    private String registerToken;
    private String loginToken;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegisterToken() {
        return registerToken;
    }

    public void setRegisterToken(String token) {
        this.registerToken = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }
}
