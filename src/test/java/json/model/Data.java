package json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Marius Dima on 05.09.2019.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    @JsonProperty("data")
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
