package configuration;

import backpack.TestBackpack;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 * Created by Marius Dima on 29.08.2019.
 */
@Configuration
@PropertySource("classpath:automation.properties")
@ComponentScan(basePackages = {"backpack", "steps"})
public class AutomationConfig {

    @Bean
    @Profile("chrome")
    public WebDriver getChrome() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chrome/chromedriver-75.exe");
        System.getProperty("browser.url");
        return new ChromeDriver();
    }

    @Bean
    @Profile("api")
    public WebDriver driver() {
        return null;
    }


    @Bean
    @Profile("firefox")
    public WebDriver getFireFox() {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/firefox/geckodriver-0.24.exe");

        return new FirefoxDriver();
    }

    @Value("${browser.url:}")
    private String url;

    @Bean
    @Profile("ie")
    public WebDriver getIEDriver() {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/ie/IEDriverServer-x86-3.14.exe");

        InternetExplorerOptions options = new InternetExplorerOptions().withInitialBrowserUrl(url);
        WebDriver driver = new InternetExplorerDriver(options);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return driver;
    }

}
