Feature: I want to test API's
  List all users
  Get email from random user
  Register another user with the same email from above (save token to backpack)
  Login with the email and password and compare the token

  Scenario: Register user with current email
    Given I list all users from "https://reqres.in/api/users"
    And I save a random user email
    When I register a new user at "https://reqres.in/api/register"
    And I login with the newly created user at "https://reqres.in/api/login"
    Then The resulted token from login should be the same with the register token
